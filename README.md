# Python víkend s kiwi soutěžní příklad

## run

```shell
# ./book_flight.sh
```

nebo

```shell
# ./src/book_flight.py
```

## test

```shell
# pytest
```

## poznámky

Program se poměrně strikně drží zadání. Vrací tedy vždy jen jeden let, nejlevnější, nebo nejrychlejší. Situaci, kdy například požaduji nejlevnější let, ale druhý nejlevnější bude třeba jen o pouhou hodinu rychlejší a jen o jedno euro dražší program neřeší. 

Snažil jsem se alespoň při zadávání údajů o cestujicím umožnit opravit případné chyby při zadání. U vyhledání letu jsem to nepovažoval za potřebné, program lze spustit znovu s upravenými parametry (ale jak jsem psal výše, moc to v některých situacích nepomůže, program ze serveru natvrdo stáhne jen první záznam).

V kódu je několik vývojářských poznámek označených jako `@TODO: msg`. Označují vesměs místa, kam jsem se chtěl ještě vrátit a případně kód upravit. Jejich seznam lze získat příkazem `grep @TODO src/*`, ale myslím, že tuto syntaxi umí i různá IDE a nástroje pro dokumentaci. 
Chtěl jsem ale především říci to, že v těch TODO poznámkách nejsou většinou věci na vylepšení programu, další funkčnost atd. Některé poznámky nejsou příliš srozumitelné, spíš říkají "tímhle se teď zdržovat nebudu pak se na to (možná) podívám". To slovíčko "možná" se v praxi ukázalo jako důležité.

Testy jsem příliš nedělal. Ne proto, že je nepovažuji za důležité. Program využívá dvě REST API, které jsem testoval v rozšíření pro prohlížeč a nemá úplně smysl je dávat do testů. Přesto jsem test na booking api udělal. Bude fungovat, dokud se API nezmění, dokud bude platný ten klíč s názvem `booking_token` apod.

Nevytvářel jsem pythonní balíček, ač v souboru install.sh počítám s tím, že to balíček bude. Závislosti na dalších balíčcích jsou v souborech *requirements.txt* a *requirements_dev.txt*. Zbývá přidat jen *setup.py*. 