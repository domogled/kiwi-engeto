

from pathlib import Path
import requests

try:
    from Booking import BOOKING_URL
except ModuleNotFoundError:
    BOOKING_URL = 'http://128.199.48.38:8080/booking'


def test_booking_request():

    json_file = Path(__file__).parent / 'booking.json'

    json_data = json_file.read_text()

    print(json_data)

    resp = requests.post(BOOKING_URL,
                        data = json_data,
                        headers={'Content-Type': 'application/json'}
                        )

    assert resp.status_code == 200