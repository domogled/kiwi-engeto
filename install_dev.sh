#!/usr/bin/env sh

pip install -r requirements.txt
pip install -r requirements_dev.txt
pip install --editable .