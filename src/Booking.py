'Booking API'

import click
import re
import requests
import json

BOOKING_URL = 'http://128.199.48.38:8080/booking'

def booking(booking_token):
    'získá data cestujícího a provede rezervaci'
    
    request_data = get_booking_data(booking_token)
    
    request_json = json.dumps(request_data)
    # print(request_json)
    # exit()

    resp = requests.post(BOOKING_URL,
                        data = request_json,
                        headers={'Content-Type': 'application/json'}
                        )

    status_code = resp.status_code
    print(f'status {status_code}')

    if status_code >= 400:
        print(f'Booking failed: {resp.content}')
        return

    data = resp.json()
    print(json.dumps(data, indent=2))

    return data



def get_booking_data(booking_token: str) -> dict:
    'uživatelský vstup, potvrzuje správnost, případně opakuje'

    data = {}

    while True:
        data = prompt_data(**data)
        print('-'*42)
        
        for key, value in data.items():
            print(f'{key}: {value}')
        
        if click.confirm('Jsou zadané údaje správné?'):
            break

    passenger = {key: value for key, value in data.items() if key in ["documentID",
                            "lastName",
                            "title",
                            "firstName",
                            "email",
                            "birthday"
                            ]}   

    request_data = {
        "currency": "EUR",
        "passengers": 1,
        "booking_token": booking_token,
        "bags": data['bags'],
        "passengers": [passenger]
        }

    return request_data

def prompt_data(**kwargs):

    data = {}
    data.update(kwargs)

    # @TODO: currency option
    # @TODO: passanger numbers

    # title = click.prompt('You are Mr. or Mrs.', value_proc=lambda value: assert value in ['Mr', 'Mrs'])
    def title_validate(title):
        if not title in ['Mr', 'Mrs']:
            raise click.BadParameter('You must are Mr or Mrs :-)')

        return title

    def email_validate(email):
        if not re.match("\A(?P<name>[\w\-_]+)@(?P<domain>[\w\-_]+).(?P<toplevel>[\w]+)\Z",email,re.IGNORECASE):
            raise click.BadParameter('Email is not valid')

        return email

    def birthday_validate(birthday):
        try:
            year, month, day = birthday.split('-')
        except Exception as e:
            raise click.BadParameter(f'Birthday is not valid: {e}')

        # @TODO: valid by datetime.date???

        return f'{year}-{month}-{day}'

    click.echo('\nZadej údaje cetujicícho:')

    data['title'] = click.prompt('You are Mr. or Mrs.', value_proc=title_validate, default=data.get('title', None))
    data['firstName'] = click.prompt('Your first name', type=str, default=data.get('firstName', None))
    data['lastName'] = click.prompt('Your surname', type=str, default=data.get('lastName', None))
    data['documentID'] = click.prompt('Your document ID', type=str, default=data.get('documentID', None))
    data['email'] = click.prompt('Your email', value_proc=email_validate, default=data.get('email', None))
    data['birthday'] = click.prompt('Your birthday as YYYY-MM-DD', value_proc=birthday_validate, default=data.get('birthday', None))
    
    data['bags'] = click.prompt('Number of bags', type=int, default=data.get('bags', None))

    return data